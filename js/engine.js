$(function() {


// Бургер меню

$('.burger-menu').click(function() {
    $(this).toggleClass('active');
    $(this).next('ul').fadeToggle();
});

if ($(window).width() < '768') {
    $('.main-menu a').click(function() {
        $('.main-menu ul').fadeToggle();
        $('.burger-menu').toggleClass('active');
    });

}




// Banner-slider
$('.about-slider').slick({
    accessibility: true,
    centerMode: true,
    centerPadding: '0px',
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: true,
    responsive: [{
        breakpoint: 550,
        settings: {
            slidesToShow: 1,
        },
    }]
});


// Begin of scroll nav
var topPlus = 0
if ($(window).width() < '991'){
  topPlus = 30
}
console.log(topPlus)

$(".main-menu").on("click","a", function (event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id  = $(this).attr('href'),

    //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;
    
    //анимируем переход на расстояние - top за 1500 мс
    $('body,html').animate({scrollTop: top - topPlus}, 1500);
  });
// End of scroll nav


// FancyBox3
$('[data-fancybox]').fancybox({
    youtube : {
        showinfo : 0
    }
});


// Фиксированнаня шапка при прокрутке
$(window).scroll(function(){
    var sticky = $('header'),
        scroll = $(window).scrollTop();
    if (scroll > 200) {
        sticky.addClass('header-fixed');
    } else {
        sticky.removeClass('header-fixed');
    };
});



})